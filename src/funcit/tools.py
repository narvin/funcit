"""A the tools module in the funcit package."""

from functools import partial, reduce
from itertools import islice
from typing import Callable, Iterable, Iterator, Sequence, TypeVar

A = TypeVar("A")
B = TypeVar("B")
C = TypeVar("C")
D = TypeVar("D")


def _xxs(xxs: Iterable[A]) -> tuple[A, Iterator[A]]:
    """Split an iterable into its first element and the rest."""
    iter_xs = iter(xxs)
    return next(iter_xs), iter_xs


def foldr(f: Callable[[A, B], B], z: B, xxs: Iterable[A]) -> B:
    """Recursively combine the first element with the results of combining the rest,
    and the seed.
    """
    try:
        x, xs = _xxs(xxs)
        return f(x, foldr(f, z, xs))
    except StopIteration:
        return z


def foldr1(f: Callable[[A, A], A], xxs: Iterable[A]) -> A:
    """Recursively combine the first element with the results of combining the rest."""
    return foldr(f, *_xxs(xxs))


def foldl(f: Callable[[A, A], A], z: A, xxs: Iterable[A]) -> A:
    """Recursively combine the results of combining the seed, and all but the last
    element with the last element.
    """
    try:
        x, xs = _xxs(xxs)
        return f(foldl(f, x, xs), z)
    except StopIteration:
        return z


def foldl1(f: Callable[[A, A], A], xsx: Sequence[A]) -> A:
    """Recursively combine the results of all but the last element with the last
    element.
    """
    return foldl(f, xsx[-1], xsx[:-1])


def reduce2(f: Callable[[A, B], A], z: A, xxs: Iterable[B]) -> A:
    """Reimplement reduce."""
    try:
        x, xs = _xxs(xxs)
        return reduce2(f, f(z, x), xs)
    except StopIteration:
        return z


def curry(f: Callable[[A, B], D]) -> Callable[[B, C], D]:
    """Turn a function into a curried function."""
    return partial(f)


def apply(f: Callable[[A], B], x: A) -> B:
    """Apply a function to an argument."""
    return f(x)


def compose(f: Callable[[A], B], g: Callable[[B], C]) -> Callable[[A], C]:
    """Compose two functions together."""
    # am = partial(reduce, lambda x, f: f(x), (add2, mult3))
    # reduce = lambda f, xs, z: foldr(f, z, xs)
    # return partial(lambda f, xxs, z: foldr(f, z, xxs), lambda x, f: f(x), (f, g))
    reduce2 = lambda a, b, c: foldr(a, c, b)
    reduce2 = reduce
    return partial(reduce2, lambda x, f: f(x), (f, g))
