"""Tests for the main module in the funcit package."""

from typing import Callable, Iterable, TypeVar

import pytest

from funcit.tools import apply, compose, curry, foldl, foldr

A = TypeVar("A")
B = TypeVar("B")
C = TypeVar("C")
D = TypeVar("D")


class TestFoldr:
    """Test suite for the foldr function."""

    @staticmethod
    def test_sum_list() -> None:
        """Test that foldr sums a list."""
        assert foldr(lambda x, y: x + y, 0, (1, 2, 3)) == 6

    @staticmethod
    def test_fold_empty_list() -> None:
        """Test that foldr folds an empty list."""
        assert foldr(lambda x, y: x + y, 0, ()) == 0

    @staticmethod
    @pytest.mark.parametrize(
        "reducer, init, items, exp",
        (
            (lambda x, y: x + y, "", "abc", "abc"),
            (lambda a, x: f"({a} + {x})", "1", ("2", "3", "4"), "(1 + (2 + (3 + 4)))"),
            (lambda a, x: f"(+ {a} {x})", "1", ("2", "3", "4"), "(+ 1 (+ 2 (+ 3 4)))"),
            (lambda a, x: f"f({a}, {x})", "1", ("2", "3", "4"), "f(1, f(2, f(3, 4)))"),
        ),
    )
    def test_fold_is_rightward(
        reducer: Callable[[A, A], A], init: A, items: Iterable[A], exp: A
    ) -> None:
        """Test that foldr folds to the right."""
        assert foldr(reducer, init, items) == exp

    @staticmethod
    def test_seed_at_start() -> None:
        """Test that the seed element is at the beginning."""
        assert foldr(lambda x, y: x + y, "a", "bcd") == "abcd"


class TestFoldl:
    """Test suite for the foldl function."""

    @staticmethod
    def test_sum_list() -> None:
        """Test that foldl sums a list."""
        assert foldl(lambda x, y: x + y, 0, (1, 2, 3)) == 6

    @staticmethod
    def test_fold_empty_list() -> None:
        """Test that foldl folds an empty list."""
        assert foldl(lambda x, y: x + y, 0, ()) == 0

    @staticmethod
    @pytest.mark.parametrize(
        "reducer, init, items, exp",
        (
            (lambda x, y: x + y, "", "dog", "god"),
            (lambda a, x: f"({a} + {x})", "1", ("2", "3", "4"), "(((4 + 3) + 2) + 1)"),
            (lambda a, x: f"(+ {a} {x})", "1", ("2", "3", "4"), "(+ (+ (+ 4 3) 2) 1)"),
            (lambda a, x: f"f({a}, {x})", "1", ("2", "3", "4"), "f(f(f(4, 3), 2), 1)"),
        ),
    )
    def test_fold_is_leftward(
        reducer: Callable[[A, A], A], init: A, items: Iterable[A], exp: A
    ) -> None:
        """Test that foldl folds to the left."""
        assert foldl(reducer, init, items) == exp

    @staticmethod
    def test_seed_at_end() -> None:
        """Test that the seed element is at the end."""
        assert foldl(lambda x, y: x + y, "a", "dog") == "goda"


# class TestApply:
#     """Test suite for the apply function."""
#
#     @staticmethod
#     def test_apply_to_function() -> None:
#         """Test that apply works on a function."""
#
#         def f(x: int) -> int:
#             return x * x
#
#         assert apply(f, 5) == 25
#
#     @staticmethod
#     def test_apply_to_lambda() -> None:
#         """Test that apply works on a lambda."""
#         f = lambda x: x * x
#         assert apply(f, 5) == 25
#
#
class TestCompose:
    """Test suite for the compose function."""

    #     @staticmethod
    #     def test_compose_functions() -> None:
    #         """Test that compose works on two functions."""
    #
    #         def f(x: int) -> int:
    #             return x * x
    #
    #         def g(x: int) -> str:
    #             return f"The answer is {x}"
    #
    #         gof = compose(f, g)
    #         assert gof(5) == "The answer is 25"
    #
    #     @staticmethod
    #     def test_compose_lambdas() -> None:
    #         """Test that compose works on two lambdas."""
    #         f = lambda x: x * x
    #         g = lambda x: f"The answer is {x}"
    #         gof = compose(f, g)
    #         assert gof(5) == "The answer is 25"

    @staticmethod
    def test_compose_lambdas() -> None:
        """Test that compose works on two lambdas."""
        f = lambda x: x + 2
        g = lambda x: x * 3
        gof = compose(f, g)
        assert gof(5) == 21
