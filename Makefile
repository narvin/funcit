PACKAGE := funcit
SRC_DIR := src
TEST_DIR := test
CODE_DIRS := $(SRC_DIR) $(TEST_DIR)
DOCS_DIR := docs
DOCS_STATIC_DIR := $(DOCS_DIR)/_static
DOCS_BUILD_DIR := $(DOCS_DIR)/_build/html
INSTALL := pip install -e
SORT := python -m isort
FORMAT := python -m black
LINT := python -m pylint -s n
STYLE := python -m pycodestyle
TYPE := python -m mypy --no-error-summary
TEST := python -m pytest
DOCS := sphinx-build -b html
TAGS := ctags -R

.PHONY: all install install-all format lint style type test docs tags clean

all: lint style type format

install:
	@printf '===== INSTALL =====\n'
	@$(INSTALL) .

install-all:
	@printf '===== INSTALL w/ CHECK EXTRAS =====\n'
	@$(INSTALL) '.[code_quality,docs,test]'

format:
	@printf '===== FORMAT =====\n'
	@$(SORT) $(CODE_DIRS)
	@$(FORMAT) $(CODE_DIRS)

lint:
	@printf '===== LINT =====\n'
	@$(LINT) $(CODE_DIRS)

style:
	@printf '===== STYLE =====\n'
	@$(STYLE) $(CODE_DIRS)

type:
	@printf '===== TYPE =====\n'
	@$(TYPE) $(CODE_DIRS)

test:
	@printf '===== TEST =====\n'
	@$(TEST) $(TEST_DIR)

docs:
	@printf '===== DOCS =====\n'
	@mkdir -p $(DOCS_STATIC_DIR)
	@$(DOCS) $(DOCS_DIR) $(DOCS_BUILD_DIR)

tags:
	@printf '===== TAGS =====\n'
	@find . -type d -name site-packages | xargs $(TAGS) $(CODE_DIRS)

clean:
	@printf '===== CLEAN =====\n'
	@find $(CODE_DIRS) -type d -name __pycache__ -exec $(RM) -rf {} \+
