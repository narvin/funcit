funcit Package
==============

.. automodule:: funcit
   :members:
   :undoc-members:
   :show-inheritance:

.. toctree::
   :caption: Modules

   funcit.main <funcit.main>
